<?php namespace Drupal\cmrf_views;

use Drupal;
use Drupal\cmrf_core\Call;
use Drupal\cmrf_core\Core;
use Drupal\cmrf_views\Entity\CMRFDataset;
use Drupal\cmrf_views\Entity\CMRFDatasetRelationship;
use Drupal\cmrf_views\Util\CMRFViewsFieldNameUtil;
use Psr\Log\LoggerInterface;

class CMRFViews {

  protected Core $core;

  private LoggerInterface $logger;

  public function __construct(Core $core, LoggerInterface $logger) {
    $this->core = $core;
    $this->logger = $logger;
  }

  /**
   * Retrieve a list of entities available for Drupal Views.
   *
   * This function caches the result as the definition are build upon requesting the data
   * from the remote civicrm installation.
   *
   * When $reset is TRUE then the cache is ignored and new values are stored in the cache.
   *
   * Returns the data in the format for the hook_views_data.
   *
   * @param bool $reset
   *   Whether the cache should reset.
   *
   * @return array
   *   In format which could be used by the hook_views_data.
   */
  public function getViewsData(bool $reset = false) {
    $key = 'cmrf_views_data';
    $data = [];
    if ($cache = \Drupal::keyValue('cmrf_views')->get($key)) {
      $data = $cache;
    }

    if (empty($data) || $reset) {
      $datasets = $this->getDatasets();
      if (!empty($datasets)) {
        foreach ($datasets as $dataset_id => $dataset_prop) {
          if ((!empty($dataset_prop['connector'])) && (!empty($dataset_prop['entity']) && (!empty($dataset_prop['action'])))) {
            $dataset_prop['id'] = $dataset_id;
            $fields = $this->getFields($dataset_prop);
            // Unique identifier for this group.
            $uid = 'cmrf_views_' . $dataset_id;
            // Base data.
            $data[$uid] = $this->getBaseData($dataset_prop);
            if ((!empty($fields)) && (is_array($fields))) {
              // Fields (from the getEntityFields function).
              $data[$uid] = array_merge($fields, $data[$uid]);
            }
          }
        }
      } else {
        $data = [];
      }
      \Drupal::keyValue('cmrf_views')->set($key, $data);
    }
    return $data;
  }

  /**
   * Generate base table for views data.
   *
   * @param $dataset
   *
   * @return mixed
   */
  private function getBaseData($dataset) {
    $base_data['table'] = [];

    if (!empty($dataset['label'])) {
      $base_data['table']['group'] = $dataset['label'];
      $base_data['table']['base']  = [
        'title'    => $dataset['label'],
        'help'     => $dataset['label'] . ' provided by CiviCRM API',
        'query_id' => $dataset['api_version'] == 4 ? 'civicrm_api4' : 'civicrm_api',
      ];
    }

    if (!empty($dataset['entity'])) {
      $base_data['table']['base']['entity'] = $dataset['entity'];
    }

    if (!empty($dataset['action'])) {
      $base_data['table']['base']['action'] = $dataset['action'];
    }

    if (!empty($dataset['getcount'])) {
      $base_data['table']['base']['getcount'] = $dataset['getcount'];
    }

    if (!empty($dataset['connector'])) {
      $base_data['table']['base']['connector'] = $dataset['connector'];
    }

    if (!empty($dataset['params'])) {
      $base_data['table']['base']['params'] = $dataset['params'];
    }

    if (!empty($dataset['api_version'])) {
      $base_data['table']['base']['api_version'] = $dataset['api_version'];
    }

    return $base_data;
  }

  /**
   * Retrieve all the fields for an entity in the form of Drupal views.
   *
   * @return array<string, mixed>
   */
  public function getFields($dataset): array {

    if ((!empty($dataset['connector'])) && (!empty($dataset['entity'])) && (!empty($dataset['action']))) {

      // Set the parameters from the dataset params options.
      if (!empty($dataset['params'])) {
        array_walk_recursive($dataset['params'], ['Drupal\cmrf_views\CMRFViews', 'tokenReplace']);
      }

      // API Call to retrieve the fields.
      if ($dataset['api_version'] == 4) {
        $parameters = [
          'action' => $dataset['action'],
          'loadOptions' => TRUE,
        ];
      }
      else {
        $parameters = [
          'api_action' => $dataset['action'],
        ];
      }
      $call = $this->core->createCall(
        $dataset['connector'],
        $dataset['entity'],
        $dataset['getfields'],
        $parameters + $dataset['params'],
        ['limit' => 0],
        NULL,
        $dataset['api_version']
      );
      $this->core->executeCall($call);
      if ($call->getStatus() != Call::STATUS_DONE) {
        return [];
      }

      // Get fields value.
      $fields = $call->getReply();
      if (empty($fields['values'])) {
        return [];
      }
      $apiVersion = $call->getApiVersion();

      if ($apiVersion == 4) {
        $fields['values'] = array_combine(array_column($fields['values'], 'name'), $fields['values']);
      }

      // Retrieve available relationships available for the current dataset.
      $dataset_relationships = CMRFDatasetRelationship::loadByDataset($dataset['id']);

      // Loop through each field to create the appropriate structure for views data.
      $views_fields = [];
      foreach ($fields['values'] as $field_name => $field_prop) {
        $original_field_name = $field_name;
        $field_name = CMRFViewsFieldNameUtil::normalize($field_name);
        if (isset($views_fields[$field_name])) {
          $this->logger->warning(
            'The CiviCRM fields "@firstFieldName" and "@secondFieldName" of entity "@entityName" are mapped to '
            . 'the same Views field "@normalizedFieldName". The second field won\'t be available in the View.',
            [
              '@firstFieldName' => $views_fields[$field_name]['cmrf_original_definition']['name'],
              '@secondFieldName' => $original_field_name,
              '@entityName' => $dataset['entity'],
              '@normalizedFieldName' => $field_name,
            ]
          );
          continue;
        }

        $field_prop['api.version'] = $apiVersion;

        // If we don't have a field type, set it to 0.
        if ($apiVersion == 3 && !isset($field_prop['type'])) {
          $field_prop['type'] = 0;
        }
        if ($apiVersion == 4 && !isset($field_prop['data_type'])) {
          $field_prop['data_type'] = NULL;
        }

        // Set default for "api.filter".
        if (!isset($field_prop['api.filter'])) {
          $field_prop['api.filter'] = 1;
        }
        // Set default for "api.sort".
        if (!isset($field_prop['api.sort'])) {
          $field_prop['api.sort'] = 1;
        }

        // Set field handler, filter, sort, etc.
        if ($apiVersion == 3) {
          switch ($field_prop['type']) {
            case 1: // Integer field.
            case 1024: // Money field.
              $views_fields[$field_name] = $this->getNumericField($field_prop);
              break;
            case 4: // Date field.
            case 12: // Date and time field.
            case 256: // Timestamp field.
              $views_fields[$field_name] = $this->getDateField($field_prop);
              break;
            case 16: // Boolean field.
              $views_fields[$field_name] = $this->getBooleanField($field_prop);
              break;
            case 32: // Markup field.
              $views_fields[$field_name] = $this->getMarkupField($field_prop);
              break;
            case 2: // String field
              if (!empty($field_prop['format']) && $field_prop['format'] == 'json') {
                $views_fields[$field_name] = $this->getJSONField($field_prop);
                break;
              }
            // No "break" statement for other string types falling through.
            default: // Fallback standard field.
              $views_fields[$field_name] = $this->getStandardField($field_prop);
              break;
          }
        }
        elseif ($apiVersion == 4) {
          switch ($field_prop['data_type']) {
            case 'Int':
            case 'Integer':
            case 'Float':
            case 'Money':
              $views_fields[$field_name] = $this->getNumericField($field_prop);
              break;
            case 'Date':
            case 'Timestamp':
              $views_fields[$field_name] = $this->getDateField($field_prop);
              break;
            case 'Boolean':
              $views_fields[$field_name] = $this->getBooleanField($field_prop);
              break;
            case 'Text':
              $views_fields[$field_name] = $this->getMarkupField($field_prop);
              break;
            case 'String':
              if (!empty($field_prop['format']) && $field_prop['format'] == 'json') {
                $views_fields[$field_name] = $this->getJSONField($field_prop);
                break;
              }
            // TODO: case 'Array'?
            // No "break" statement for other string types falling through.
            default: // Fallback standard field.
              $views_fields[$field_name] = $this->getStandardField($field_prop);
              break;
          }
        }

        // Set field basic information.
        $views_fields[$field_name]['title'] = empty($field_prop['title']) ? '' : $field_prop['title'];
        $views_fields[$field_name]['help']  = empty($field_prop['description']) ? '' : $field_prop['description'];
        $views_fields[$field_name]['group'] = $dataset['label'];
        $views_fields[$field_name]['cmrf_original_definition'] = $field_prop;

        // Make sorting use the correct field names, i.e. without dots replaced.
        if (!empty($views_fields[$field_name]['sort'])) {
          $views_fields[$field_name]['sort']['field'] = $original_field_name;
        }

        // Set click sortable to 'true' by default.
        $views_fields[$field_name]['field']['click sortable'] = TRUE;

        // Set whether the field contains multiple items.
        if (!empty($field_prop['serialize'])) {
          $views_fields[$field_name]['field']['multiple'] = 1;
          $views_fields[$field_name]['field']['click sortable'] = FALSE;
        }

        // Add relationship properties when configured for this field.
        foreach ($dataset_relationships as $dataset_relationship) {
          if ($dataset_relationship->referencing_key == $field_name) {
            $views_fields[$field_name]['relationship'] = [
              'base' => 'cmrf_views_' . $dataset_relationship->referenced_dataset,
              'base field' => $dataset_relationship->referenced_key,
              'id' => 'cmrf_dataset_relationship',
              'label' => $dataset_relationship->label,
              'cmrf_dataset_relationship' => $dataset_relationship->id,
              'relationship table' => 'cmrf_views_' . $dataset_relationship->referenced_dataset,
              'relationship field' => $dataset_relationship->referenced_key,
            ];
          }
        }
      }

      return $views_fields;
    }

    return [];
  }

  /**
   * Generate numeric field for views data.
   *
   * @param $prop
   *
   * @return mixed
   */
  private function getNumericField($prop) {

    if (!empty($prop['options'])){
      $field['field']['options'] = $prop['options'];
      $field['field']['multiple'] = TRUE;
      $field['field']['id']    = 'cmrf_views_optionlist';
    } else {
      $field['field']['id'] = 'numeric';
    }
    if (!empty($prop['api.sort'])) {
      $field['sort']['id'] = 'standard';
    }

    // If 'type' is 1024 (Money).
    if (
      ($prop['api.version'] == 3 && !empty($prop['data_type']) && ($prop['type'] == 1024))
      || ($prop['api.version'] == 4 && ($prop['data_type'] == 'Money' || $prop['data_type'] == 'Float'))
    ) {
      $field['field']['float'] = TRUE;
    }

    // Add filter to the field.
    if (!empty($prop['api.filter'])) {
      $field['argument']['id'] = 'cmrf_views_argument_standard';
      if (!empty($prop['options'])) {
        $field['filter']['id'] = 'cmrf_views_filter_optionlist';
        $field['filter']['options'] = $prop['options'];
      } else {
        if (
          ($prop['api.version'] == 3 && $prop['type'] == 1024)
          || ($prop['api.version'] == 4 && ($prop['data_type'] == 'Money' || $prop['data_type'] == 'Float'))
        ) {
          $field['filter']['id'] = 'cmrf_views_filter_text';
        }
        else {
          $field['filter']['id'] = 'cmrf_views_filter_numeric';
        }
      }
    }

    // If 'input_type' is file.
    if (
      ($prop['api.version'] == 3 && !empty($prop['data_type']) && $prop['data_type'] == 'File')
      || ($prop['api.version'] == 4 && !empty($prop['input_type']) && $prop['input_type'] == 'File')
    ) {
      $field['field']['id'] = 'cmrf_views_file';
    }

    return $field;
  }

  /**
   * Generate date field for views data.
   *
   * @param $prop
   *
   * @return mixed
   */
  private function getDateField($prop) {

    // Default.
    $field['field']['id']    = 'cmrf_views_date';
    if (!empty($prop['api.sort'])) {
      $field['sort']['id'] = 'standard';
    }

    // Add filter to the field.
    if (!empty($prop['api.filter'])) {
      $field['argument']['id'] = 'date';
      $field['filter']['id'] = 'cmrf_views_filter_date';
      if (!empty($prop['options'])) {
        $field['filter']['id']      = 'cmrf_views_filter_optionlist';
        $field['filter']['options'] = $prop['options'];
      }
    }

    return $field;
  }

  /**
   * Generate boolean field for views data.
   *
   * @param $prop
   *
   * @return mixed
   */
  private function getBooleanField($prop) {

    // Default.
    $field['field']['id']    = 'boolean';
    if (!empty($prop['api.sort'])) {
      $field['sort']['id'] = 'standard';
    }

    // Add filter to the field.
    if (!empty($prop['api.filter'])) {
      $field['argument']['id'] = 'date';
      $field['filter']['id'] = 'cmrf_views_filter_boolean';
    }

    // TODO: Check 'use equal' and 'options'
    //if ($filterField) {
    //  $field['filter']['id'] = 'cmrf_views_handler_filter_boolean_operator';
    //  $field['filter']['use equal'] = TRUE;
    //  $field['filter']['options'] = $fieldOtions;
    //}

    return $field;
  }

  /**
   * Generate markup field for views data.
   *
   * @param $prop
   *
   * @return mixed
   */
  private function getMarkupField($prop) {

    // Default.
    $field['field']['id']    = 'cmrf_views_markup';
    if (!empty($prop['api.sort'])) {
      $field['sort']['id'] = 'standard';
    }

    // Add filter to the field.
    if (!empty($prop['api.filter'])) {
      $field['argument']['id'] = 'cmrf_views_argument_standard';
      $field['filter']['id'] = 'cmrf_views_filter_text';
      if (!empty($prop['options'])) {
        $field['filter']['id']      = 'cmrf_views_filter_optionlist';
        $field['filter']['options'] = $prop['options'];
      }
    }

    // If 'input_type' is file.
    if (
      ($prop['api.version'] == 3 && !empty($prop['data_type']) && $prop['data_type'] == 'File')
      || ($prop['api.version'] == 4 && !empty($prop['input_type']) && $prop['input_type'] == 'File')
    ) {
      $field['field']['id'] = 'cmrf_views_file';
    }

    return $field;
  }

  /**
   * Generates JSON field for views data.
   *
   * @param $prop
   */
  private function getJSONField($prop) {
    $field['field']['id'] = 'cmrf_views_json';
    $field['field']['multiple'] = 1;

    return $field;
  }

  /**
   * Generate standard field for views data.
   *
   * @param $prop
   *
   * @return mixed
   */
  private function getStandardField($prop) {

    // Default.
    if (!empty($prop['options'])){
      $field['field']['options'] = $prop['options'];
      $field['field']['multiple'] = TRUE;
      $field['field']['id']    = 'cmrf_views_optionlist';
    } else {
      $field['field']['id'] = 'cmrf_views_standard';
    }

    if ($prop['api.version'] == 4 && !empty($prop['input_attrs']['multiple'])) {
      $field['field']['multiple'] = TRUE;
    }

    if (!empty($prop['api.sort'])) {
      $field['sort']['id'] = 'standard';
    }
    if (!empty($prop['options'])){
      $field['field']['options'] = $prop['options'];
    }

    // Add filter to the field.
    if (!empty($prop['api.filter'])) {
      $field['argument']['id'] = 'cmrf_views_argument_standard';
      $field['filter']['id'] = 'cmrf_views_filter_text';
      if (!empty($prop['options'])) {
        $field['filter']['id']      = 'cmrf_views_filter_optionlist';
        $field['filter']['options'] = $prop['options'];
      }
    }

    // If 'input_type' is file.
    if (
      ($prop['api.version'] == 3 && !empty($prop['data_type']) && $prop['data_type'] == 'File')
      || ($prop['api.version'] == 4 && !empty($prop['input_type']) && $prop['input_type'] == 'File')
    ) {
      $field['field']['id'] = 'cmrf_views_file';
    }

    return $field;
  }

  /**
   * Fetch field options.
   *
   * @param $connector
   * @param $api_entity
   * @param $api_action
   * @param $field_name
   *
   * @return array
   *
   * TODO: Method does not seem to be called from anywhere. Deprecate or remove?
   */
  private function fetchOptions($connector, $api_entity, $api_action, $field_name) {

    // Get field options API call.
    $call = $this->core->createCall(
      $connector,
      $api_entity,
      'getoptions',
      ['field' => $field_name],
      ['limit' => 0, 'cache' => '5 minutes']
      // TODO: API version from Dataset configuration.
    );

    // Execute call.
    $this->core->executeCall($call);
    if ($call->getStatus() == Call::STATUS_DONE) {
      $optionResult = $call->getReply();

      if (isset($optionResult['values']) && is_array($optionResult['values'])) {
        return $optionResult['values'];
      }
    }

    // Get fields API call.
    $call = $this->core->createCall(
      $connector,
      $api_entity,
      'getfields', // TODO: Use "getfields" property of the Dataset?
      ['api_action' => $api_action],
      ['limit' => 0]
      // TODO: API version from Dataset configuration.
    );

    // Execute call.
    $this->core->executeCall($call);
    if ($call->getStatus() == Call::STATUS_DONE) {
      $fields = $call->getReply();
      if (isset($fields['values']) && is_array($fields['values']) && isset($fields['values'][$field_name]) && isset($fields['values'][$field_name]['options']) && is_array($fields['values'][$field_name]['options'])) {
        return $fields['values'][$field_name]['options'];
      }
    }
    return [];
  }

  /**
   * Get views datasets from the config entity.
   *
   * @return array
   */
  public function getDatasets() {
    $return = [];
    foreach (CMRFDataset::loadMultiple() as $dataset_id => $dataset) {
      $return[$dataset_id] = $dataset->toArray();
      $return[$dataset_id]['params'] = json_decode($return[$dataset_id]['params'], TRUE);
      if ($return[$dataset_id]['params'] === FALSE) {
        $return[$dataset_id]['params'] = [];
      }
    }
    return $return;
  }

  /**
   * Replace tokens in a given value.
   *
   * @param string $value
   *   The value wich to replace tokens in.
   *
   * @param string
   *   The value with tokens replaced.
   */
  public static function tokenReplace(&$value) {
    $value = Drupal::token()->replace($value);
  }

}
